--FixedLerp
local function FixedLerp(a, b, w)
    return FixedMul((FRACUNIT - w), a) + FixedMul(w, b)
end

addHook("ThinkFrame", function()
	for p in players.iterate
	
		if p.mo.skin == "sms" or p.mo.skin == "mario" or p.mo.skin == "luigi"
			continue 
		end

		if (p.charability2 == CA2_SPINDASH or p.mo.state == S_PLAY_ROLL or p.mo.state == S_PLAY_SPINDASH) and p.spinitem
			p.spinitem = 0
		end
		if (p.charability == CA_HOMINGTHOK) and p.thokitem
			p.thokitem = 0
		end
	end
end)

addHook("PostThinkFrame", function()
	for player in players.iterate()
		local mo = player.mo
		if not (mo and mo.valid) then continue end
		if (mo.skin == "inazuma") or (mo.skin == "sms")  or (mo.skin == "mario")  or (mo.skin == "luigi") or (mo.skin == "adventuresonic") or (mo.skin == "jana") or (mo.skin == "bowser") or (mo.skin == "skip") or (mo.skin == "blaze") then continue end
		if (player == nil) or (player.playerstate != PST_LIVE) then continue end
		if player.mrce and player.mrce.glowaura then continue end
		if player.charability == 18 then continue end
	
		if (mo.previousx == nil) or (mo.previousy == nil) or (mo.previousz == nil)
			mo.previousx = mo.x
			mo.previousy = mo.y
			mo.previousz = mo.z
		end
		
		if (player.speed > 15*FRACUNIT) and (player.pflags & PF_SPINNING)
		or player.homing 
			for i = 0, 9
				local percent = FRACUNIT * (i * 10) / 100
				local trail = P_SpawnMobj(mo.x, mo.y, mo.z, MT_THOK)
				local tx = FixedLerp(mo.x,mo.previousx,percent)
				local ty = FixedLerp(mo.y,mo.previousy,percent)
				local tz = FixedLerp(mo.z + 3*FRACUNIT,mo.previousz + 4*FRACUNIT,percent)
				P_TeleportMove(trail, tx, ty, tz - 7 * FRACUNIT)
				if (player.mo.skin == "shadow") and player.mo.color == SKINCOLOR_BLACK
					trail.color = SKINCOLOR_YELLOW
				elseif (player.mo.skin == "metalsonic") and player.mo.color == SKINCOLOR_COBALT
					trail.color = SKINCOLOR_PURPLE
				else
					trail.color = mo.color
				end
				trail.fuse = 13
				trail.state = S_THOK
				trail.scalespeed = FRACUNIT/12
				trail.scale = (FRACUNIT * 5/6) - (i * FRACUNIT/120)
				trail.destscale = 0
				trail.frame = TR_TRANS70|A
			end
		end

		mo.previousx = mo.x
		mo.previousy = mo.y
		mo.previousz = mo.z
	end
end)

local cv2 = CV_RegisterVar({name = "thokaura", defaultvalue = "On", flags = 0, PossibleValue = CV_OnOff, func = 0})

 local function SafeFreeslot(...)
	for _, item in ipairs({...})
		if rawget(_G, item) == nil
			freeslot(item)
		end
	end
end

SafeFreeslot(
	"MT_FAKETHOK",
	"MT_FAKESPIN",
	"S_FAKETHOK",
	 "S_FAKESPIN",
	 "SPR_FTHK", 
	 "SPR_FSPN")
	 
//Begin thokaura
 
local thok = MT_FAKETHOK
mobjinfo[thok].spawnstate = S_FAKETHOK
mobjinfo[thok].health = 1000
mobjinfo[thok].radius = 20*FRACUNIT
mobjinfo[thok].height = 41*FRACUNIT
mobjinfo[thok].speed = 8
mobjinfo[thok].flags = MF_SCENERY|MF_NOGRAVITY|MF_NOCLIP
mobjinfo[thok].dispoffset = 1
 
local spin = MT_FAKESPIN
mobjinfo[spin].spawnstate = S_FAKESPIN
mobjinfo[spin].health = 1000
mobjinfo[spin].radius = 20*FRACUNIT
mobjinfo[spin].height = 41*FRACUNIT
mobjinfo[spin].speed = 8
mobjinfo[spin].flags = MF_SCENERY|MF_NOGRAVITY|MF_NOCLIP
mobjinfo[spin].dispoffset = 1
 
states[S_FAKETHOK].sprite = SPR_FTHK
states[S_FAKETHOK].frame = A|FF_TRANS60
states[S_FAKETHOK].tics = 1
states[S_FAKETHOK].nextstate = S_NULL
 
states[S_FAKESPIN].sprite = SPR_FSPN
states[S_FAKESPIN].frame = A|FF_TRANS60
states[S_FAKESPIN].tics = 1
states[S_FAKESPIN].nextstate = S_NULL
 
addHook("ThinkFrame", function(p)
	for p in players.iterate
		if cv2.value == 1
			if p.mrce and p.mrce.glowaura continue end
			if (p.mo.skin == "sonic") or (p.mo.skin == "metalsonic") or (p.mo.skin == "shadow")
				if (p.mo.state == S_PLAY_JUMP)
					local fakethok = P_SpawnMobjFromMobj(p.mo, 0*FRACUNIT, 0*FRACUNIT, -3*FRACUNIT, MT_FAKETHOK)
					if (p.mo.skin == "shadow") and (p.mo.color == SKINCOLOR_BLACK)
						fakethok.color = SKINCOLOR_YELLOW
					elseif (p.mo.skin == "metalsonic") and (p.mo.color == SKINCOLOR_COBALT)
						fakethok.color = SKINCOLOR_PURPLE
					else
						fakethok.color = p.mo.color
					end
					if (p.mo.flags2 & MF2_OBJECTFLIP) and (p.mo.eflags & MFE_VERTICALFLIP)
						fakethok.flags2 = $ | MF2_OBJECTFLIP
						fakethok.eflags = $ | MFE_VERTICALFLIP
					end
				end
			end
		end
	end
end)
 
addHook("ThinkFrame", function(p) --idk why do I need to spawn different ones when jumping/rolling but that's it
	for p in players.iterate
		if cv2.value == 1
			if p.mrce and p.mrce.glowaura continue end
			if (p.mo.skin == "sonic") or (p.mo.skin == "metalsonic") or (p.mo.skin == "shadow")
				if (p.mo.state == S_PLAY_ROLL)
					local fakethok = P_SpawnMobjFromMobj(p.mo, 0*FRACUNIT, 0*FRACUNIT, -3*FRACUNIT, MT_FAKETHOK)
					if (p.mo.skin == "shadow") and (p.mo.color == SKINCOLOR_BLACK)
						fakethok.color = SKINCOLOR_YELLOW
					elseif (p.mo.skin == "metalsonic") and (p.mo.color == SKINCOLOR_COBALT)
						fakethok.color = SKINCOLOR_PURPLE
					else
						fakethok.color = p.mo.color
					end
					if (p.mo.flags2 & MF2_OBJECTFLIP) and (p.mo.eflags & MFE_VERTICALFLIP)
						fakethok.flags2 = $ | MF2_OBJECTFLIP
						fakethok.eflags = $ | MFE_VERTICALFLIP
					end
				end
			end
		end
	end
end)
 
addHook("ThinkFrame", function(p) --for spinning
	for p in players.iterate
		if cv2.value == 1
			if p.mrce and p.mrce.glowaura continue end
			if (p.mo.skin == "sonic") or (p.mo.skin == "metalsonic") or (p.mo.skin == "shadow")
				if (p.mo.state == S_PLAY_SPINDASH)
					local fakespin = P_SpawnMobjFromMobj(p.mo, 0*FRACUNIT, 0*FRACUNIT, -3*FRACUNIT, MT_FAKESPIN)
					if (p.mo.skin == "shadow") and (p.mo.color == SKINCOLOR_BLACK)
						fakespin.color = SKINCOLOR_YELLOW
					elseif (p.mo.skin == "metalsonic") and (p.mo.color == SKINCOLOR_COBALT)
						fakespin.color = SKINCOLOR_PURPLE
					else
						fakespin.color = p.mo.color
					end
					if (p.mo.flags2 & MF2_OBJECTFLIP) and (p.mo.eflags & MFE_VERTICALFLIP)
						fakespin.flags2 = $ | MF2_OBJECTFLIP
						fakespin.eflags = $ | MFE_VERTICALFLIP
					end
					fakespin.angle = p.drawangle
				end
			end
		end
	end
end)
/*
Episode Selection Menu

Used for selection between second quest and regular mode
after game complete

Original menu framework taken from Ultimate Randomizer
A lot has been modified from that original script

Created by Ashi
OG by Nightwolf
*/


addHook("MapLoad", function(p)
	for player in players.iterate
		if gamemap == 99 then
			player.mrhubon = true
		end
		if gamemap != 99 then
			player.mrhubon = false
		end
	end
end)

local HUDX = 0
local HUDY = 0
local HUDWINWIDTH = 320 //Maximum 320 (actual size is 4 units smaller due to the outline)
local HUDWINHEIGHT = 200 //Maximum 200
local BGHEIGHT = 200
local HUDTEXTHEIGHT = 30 //Height between HUD texts
local selx = 0
local sely = 0
local textwidth = 0

local HBUTTON_UP = 1
local HBUTTON_DOWN = 2
local HBUTTON_JUMP = 3
local HBUTTON_SPIN = 4

-- ease animation vars
local text_animdur = 1*TICRATE
local text_maxslide = 160
local text_animtime = 0

local animtime_capped = nil
local animate_percentage = nil

local function listnext(list)
	if type(list) != "table" return end
	if not list.selection then list.selection = 1 end
	if list.selection < #list.contents then
		list.selection = $1 + 1
	else
		list.selection = 1
	end
	list.selsound()
end

local function listprev(list)
	if type(list) != "table" return end
	if not list.selection then list.selection = 1 end
	if list.selection < 2 then
		list.selection = #list.contents
	else
		list.selection = $1 - 1
	end
	list.selsound()
end

local function clistmove(num,limit,fwd)
	if not num or not limit then return 1 end
	if fwd then
		if num < limit then return num+1 else return 1 end
	else
		if num < 2 then return limit else return num-1 end
	end
end

local function ishbuttonreleased(p,button)
	if button == HBUTTON_UP then
		if p.cmd.forwardmove <= 35 then return true end
	elseif button == HBUTTON_DOWN then
		if p.cmd.forwardmove >= -35 then return true end
	elseif button == HBUTTON_JUMP then
		if p.cmd.buttons & BT_JUMP == 0 then return true end
	elseif button == HBUTTON_SPIN then
		if p.cmd.buttons & BT_USE == 0 then return true end
	end
	return false
end

local function reversenum(tab,index)
	if not tab then return end
	if not index then return end
	if tab[index] == nil then return end
	if tab[index] == 0 or tab[index] == false
		tab[index] = 1
		return
	else
		tab[index] = 0
		return
	end
end

local function changelnum(huddata,num)
	if not huddata then return end
	if not num then num = 1 end
	huddata.listnum = num
	huddata.selection = 1
end

local function hudtimegoleft(huddata,self,time,minsec,maxmin)
	local tsec = time % 60
	local tmin = time / 60
	if self.selpart < 1 then return time end
	if self.issel then
		if self.selpart == 1 then 
			tmin = clistmove(tmin+1,maxmin,false)-1
			if tmin == 0 and tsec < minsec then tsec = 1 end
		elseif self.selpart == 2 then 
			if tmin == 0 then tsec = clistmove(tsec-minsec+1,60,false)+minsec-1 % 60
			else tsec = clistmove(tsec+1,60,false)-1 end
		end
	else 
		self.selpart = clistmove(self.selpart,2,false) 
	end
	return tmin * 60 + tsec
end

local function hudtimegoright(huddata,self,time,minsec,maxmin)
	local tsec = time % 60
	local tmin = time / 60
	if self.selpart < 1 then return time end
	if self.issel then
		if self.selpart == 1 then 
			tmin = clistmove(tmin+1,maxmin,true)-1
			if tmin == 0 and tsec < minsec then tsec = 1 end
		elseif self.selpart == 2 then
			if tmin == 0 then tsec = clistmove(tsec-minsec+1,60,true)+minsec-1 % 60
			else tsec = clistmove(tsec+1,60,true)-1 end
		end
	else 
		self.selpart = clistmove(self.selpart,2,true) 
	end
	return tmin * 60 + tsec
end



local hudcontents = {}
hudcontents[1] = {center = 2}
hudcontents[1][1] = {
	startstring = "Episode Select",
	string = "Lock-On",
	go = function(huddata, p)
		print("Starting Lock On!")
		G_SetCustomExitVars(1,1)
		huddata.selection = -1
		G_ExitLevel()
	end,
	back = function()
		return
	end,
}
hudcontents[1][2] = {
	string = "Main Quest",
	go = function(huddata, p)
		print("Starting Main Quest!")
		G_SetCustomExitVars(101,1)
		huddata.selection = -1
		G_ExitLevel()
	end,
	back = function()
		return
	end,
}
hudcontents[1][3] = {
	string = "Second Quest",
	go = function(huddata)
		print("Starting Second Quest!")
		G_SetCustomExitVars(101,1)
		huddata.selection = -1
		mrce_secondquest = true
		G_ExitLevel()
	end,
	back = function()
		return
	end,
}

rawset(_G, "MRCE_addEpisode", function(newentry)
	table.insert(hudcontents[1], newentry)
end)

//Credit to Flame for the 3 lines of code below
addHook("JumpSpecial", function(p) return p.mrhubon end)
addHook("SpinSpecial", function(p) return p.mrhubon end)
addHook("JumpSpinSpecial", function(p) return p.mrhubon end)

addHook("PlayerThink",function(p)
	if not p.mrhubon then return end
	if not p.mrhub then 
		p.mrhub = {} 
		p.mrhub.selection = 2
		p.mrhub.listnum = 1
		p.mrhub.pressed = 0
		p.mrhub.sidepress = false //Left and right presses are checked differently as L/R functions require constant input
		p.mrhub.selsound = function()
			S_StartSound(nil,sfx_menu1,p)
		end
	end
	p.mrhub.contents = hudcontents[p.mrhub.listnum]
	local sel = p.mrhub.selection
	local cont = p.mrhub.contents
	local sidemove = p.cmd.sidemove
	local fwdmove = p.cmd.forwardmove
	if p.mo then
		p.mo.momx = 0
		p.mo.momy = 0
	end
	if sidemove > 35 then
		if p.mrhub.sidepress then p.mrhub.sidepress = false; return end
		if cont[sel].right then cont[sel].right(p.mrhub,cont[sel]) end
		p.mrhub.sidepress = true
	elseif sidemove < -35 then
		if p.mrhub.sidepress then p.mrhub.sidepress = false; return end
		if cont[sel].left then cont[sel].left(p.mrhub,cont[sel]) end
		p.mrhub.sidepress = true
	elseif p.mrhub.pressed then
		if ishbuttonreleased(p,p.mrhub.pressed) then p.mrhub.pressed = 0 end
		return
	elseif fwdmove > 35 then
		p.mrhub.pressed = HBUTTON_UP
		if cont[sel].up then cont[sel].up(p.mrhub,cont[sel]) else listprev(p.mrhub) selx = HUDX+4 end
	elseif fwdmove < -35 then
		p.mrhub.pressed = HBUTTON_DOWN
		if cont[sel].down then cont[sel].down(p.mrhub,cont[sel]) else listnext(p.mrhub) selx = HUDX+4 end
	elseif p.cmd.buttons & BT_JUMP != 0 then
		p.mrhub.pressed = HBUTTON_JUMP
		if cont[sel].go then cont[sel].go(p.mrhub,cont[sel]) end
	elseif p.cmd.buttons & BT_USE != 0 then
		p.mrhub.pressed = HBUTTON_SPIN
		if cont[sel].back then cont[sel].back(p.mrhub,cont[sel]) end
	end
end)

hud.add(function(v,p,cam)
	if not p.mrhubon or not p.mrhub then return end
	if not p.mrhub.selection or not p.mrhub.contents then return end
	if p.mrhub.selection < 1 then return end
	v.drawFill(HUDX,HUDY,HUDWINWIDTH,HUDWINHEIGHT,159)
	v.draw(30,BGHEIGHT+10,v.cachePatch("MYSTIC1"))
	v.draw(290,BGHEIGHT+10,v.cachePatch("MYSTIC1"),V_FLIP)
	v.draw(30,BGHEIGHT-310,v.cachePatch("MYSTIC1"))
	v.draw(290,BGHEIGHT-310,v.cachePatch("MYSTIC1"),V_FLIP)
	v.drawFill(-170,-100,200,400,31)
	v.drawFill(290,-100,200,400,31)
	local textx = 160 //X of value description (left border)
	if selx == 0 then
		selx = HUDX+4 //X of the value selected
	end
	local startstr = hudcontents[p.mrhub.listnum][1].startstring
	local arrangenum = hudcontents[p.mrhub.listnum].center
	local valuex = HUDX+HUDWINWIDTH-4 //X of value string (right border)
	local starttexty = HUDY+4 //Start Y of HUD text
	if p.mrhub.contents[p.mrhub.selection].startstring then startstr = p.mrhub.contents[p.mrhub.selection].startstring end
	v.drawString(HUDX+(HUDWINWIDTH/2),starttexty,"\x88"..tostring(startstr),0,"center")
	local elemy = starttexty+HUDTEXTHEIGHT //Start Y of first interactive element (changes in the next for loop)
	if sely == 0 then
		sely = elemy
	end
	local elemlimit = #p.mrhub.contents
	for f = 1,elemlimit do
		if p.mrhub.selection < f then
			if elemy == 90+HUDTEXTHEIGHT then
				elemy = $1+HUDTEXTHEIGHT
			else
				elemy = 90+HUDTEXTHEIGHT
			end
		elseif p.mrhub.selection > f then
			if p.mrhub.selection-f == 1 then
				elemy = $1+HUDTEXTHEIGHT
			else
				elemy = $1+((p.mrhub.selection-f)/HUDTEXTHEIGHT)
			end
		end

		if p.mrhub.selection == f then 
			v.drawNameTag(textx-(v.nameTagWidth(p.mrhub.contents[f].string)/2),90,p.mrhub.contents[f].string,0,SKINCOLOR_WHITE,SKINCOLOR_EMERALD)
			textwidth = v.nameTagWidth(p.mrhub.contents[f].string)
		elseif (((p.mrhub.selection - 3) < f) and ((p.mrhub.selection + 3) > f))
			v.drawScaledNameTag(((textx-v.nameTagWidth(p.mrhub.contents[f].string)/FRACUNIT/2)*FRACUNIT/2)+40*FRACUNIT,elemy*(FRACUNIT),p.mrhub.contents[f].string,0,FRACUNIT/2,SKINCOLOR_WHITE, SKINCOLOR_BLACK) 
		end
		v.draw(textx-(textwidth/2)-23, 90, v.cachePatch("CHAOS3"))
		v.draw(textx+(textwidth/2)+10, 90, v.cachePatch("CHAOS3"))
		if p.mrhub.contents[f].printstr then 
			v.drawString(valuex,elemy,p.mrhub.contents[f].printstr,0,"right") 
		end
	end
end,"game")

addHook("ThinkFrame", do
	for player in players.iterate do
		if not player.mrhubon or not player.mrhub then return end
		if BGHEIGHT == 220 then
			BGHEIGHT = -100
		else
			BGHEIGHT = $ + 2
		end
	end
end)

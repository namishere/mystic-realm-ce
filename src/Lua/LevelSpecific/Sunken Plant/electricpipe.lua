/*
	Electric pipe gimmick

	(C) 2022 by AshiFolfi
*/

local epipe_ticker = 0
local is_player = false

-- Electric pipe effect
---@param line nil
---@param trigmob mobj_t
---@param sector sector_t
local function sp_elecpipe_start(line , trigmob, sector)

	if (epipe_ticker >= 35) then
		for mobj in mobjs.iterate() do
			if mobj.valid and mobj.type == 751
			and mobj.spawnpoint.angle == 112 then
				P_TeleportMove(trigmob, mobj.x, mobj.y, mobj.z+trigmob.height)
			end
		end
	end

	if (epipe_ticker >= 15)
	and not(trigmob.spritexscale == 0)
	and not(trigmob.spriteyscale == 0) then
		-- Change the sprite scale
		-- shrink x scale
		trigmob.spritexscale = $ - (FRACUNIT/4)

		-- shrink y scale slower than x
		trigmob.spriteyscale = $ - (FRACUNIT/8)
	end

	-- stop all momentum
	trigmob.momx = 0
	trigmob.momy = 0
	trigmob.momz = 0

	-- Check if it's a player
	if (trigmob.player) then
		-- Transition into spin state
		trigmob.state = S_PLAY_ROLL

		-- Set a variable to block KeyDown
		is_player = true
	else
		is_player = false
	end

	-- increment the ticker
	epipe_ticker = $ + 1

	print(epipe_ticker)
end
addHook("LinedefExecute", sp_elecpipe_start, "SP_EPSTART")

-- Electric pipe effect
---@param line nil
---@param mobj mobj_t
---@param sector nil
local function sp_elecpipe_end(line, mobj, sector)
	if (epipe_ticker < 100)
	and (epipe_ticker != 0) then
		-- Change the sprite scale
		if not(mobj.spritexscale == FRACUNIT) then
			mobj.spritexscale = $ + (FRACUNIT/4)
		end

		if not(mobj.spriteyscale == FRACUNIT) then
			mobj.spriteyscale = $ + (FRACUNIT/8)
		end

		epipe_ticker = $ + 1
		print(epipe_ticker)
	else
		epipe_ticker = 0
	end
end
addHook("LinedefExecute", sp_elecpipe_end, "SP_EPEND")

addHook("KeyDown", function(key)
	if (epipe_ticker == 0) then return end
	if (is_player) then return end

	for i=1,2 do
		if (key.num == ctrl_inputs.up[i]) or (key.num == ctrl_inputs.down[i])
		or (key.num == ctrl_inputs.left[i]) or (key.num == ctrl_inputs.right[i])
		or (key.num == ctrl_inputs.jmp[i]) or (key.num == ctrl_inputs.spn[i])
		or (key.num == ctrl_inputs.cb1[i]) or (key.num == ctrl_inputs.cb2[i])
		or (key.num == ctrl_inputs.cb3[i]) then
			return true
		end
	end
end)
--custom momentum made with a lot less hacks then CBWMom, heavily referenced from ChrispyChars. 
--Originally was gonna launch this in v2.0, but that's taking forever so i'm putting it in this minor update.
--ported from xmom v1.3 with assistance from frostiikin to suit the needs of MRCE
addHook("PreThinkFrame", function(p)
	for p in players.iterate
		if p.spectator then return end
		if not p.realmo then return end
		if p.mrce and p.mrce.customskin > 0 then --Set player.mrce.customskin to 2 every frame on your character's end to qualify as a custom skin!
			p.mrce.customskin = $-1
			p.mrce.physics = false
			return
		end
		if p.mo.skin == "adventuresonic" or p.mrce and p.mrce.physics == false then
			return
		end
		if (p.noxmomchar or p.xmomtoggledoff) then
			return 
		end
		local x = p.mrce
		if p.xmlastz == nil then
			p.xmlastspeed = x.realspeed
			p.xmlastz = p.mo.z
			p.xmlastx = p.mo.x
			p.xmlasty = p.mo.y
			p.xmlastmomx = p.mo.momx
			p.xmomlastmomy = p.mo.momy
			p.xmlaststate = p.mo.state
		end
		if p.fakenormalspeed == nil then
			p.fakenormalspeed = skins[p.mo.skin].normalspeed
		end
		local watermul = 1
		if (p.mo.eflags & MFE_UNDERWATER) then
			watermul = 2
		end
		if (p.mo and p.mo.valid and p.mo.health) then
			if p.xmlastskin and p.xmlastskin != p.mo.skin then
				p.hasnomomentum = false
			end
			p.xmlastskin = p.mo.skin
		end
		local speed = FixedDiv(FixedHypot(p.mo.momx - p.cmomx, p.mo.momy - p.cmomy), p.mo.scale)
		local SPEED_INCREASE_LEEWAY = 0*FRACUNIT // the amount of speed above normalspeed needed to update normalspeed
		local SPEED_DECREASE_LEEWAY = 15*FRACUNIT // the amount of speed below normalspeed needed to update normalspeed
			if not p.hasnomomentum and not (p.pflags & PF_SPINNING) then
				if p.restoremymomentumafteruncurlingpleasethankyouuuuu then
					p.normalspeed = max(p.restoremymomentumafteruncurlingpleasethankyouuuuu, p.fakenormalspeed)
					p.restoremymomentumafteruncurlingpleasethankyouuuuu = nil
				end
				if p.xmlastz*P_MobjFlip(p.mo) > p.mo.z*P_MobjFlip(p.mo) and P_IsObjectOnGround(p.mo) and not (p.mo.eflags & MFE_JUSTHITFLOOR) then
					p.normalspeed = $ + (p.mo.z*P_MobjFlip(p.mo)-p.xmlastz*P_MobjFlip(p.mo))/25*-1
				elseif MRCE_isHyper(p) then
					if P_IsObjectOnGround(p.mo)
						p.normalspeed = $ + FRACUNIT/12
					end
				else
				end
				local restorefakenormalspeed = p.fakenormalspeed
				if p.dashmode > 3*TICRATE
					p.fakenormalspeed = p.normalspeed
				end
				if not p.powers[pw_super] and not p.powers[pw_sneakers] then
					if (speed*watermul > p.normalspeed + SPEED_INCREASE_LEEWAY
					or speed*watermul < p.normalspeed - SPEED_DECREASE_LEEWAY) then
						p.normalspeed = max(speed*watermul, p.fakenormalspeed)
					end
				else
					if (speed*3/5*watermul > p.normalspeed + SPEED_INCREASE_LEEWAY
					or speed*3/5*watermul < p.normalspeed - SPEED_DECREASE_LEEWAY) then
							p.normalspeed = max((speed*3/5)*watermul, p.fakenormalspeed)
					end
				end 
				p.fakenormalspeed = restorefakenormalspeed
				if MRCE_isHyper(p) and not p.powers[pw_sneakers]
				or p.mo.skin == "supersonic" and (p.powers[pw_shield] & SH_FIREFLOWER) then
					if p.normalspeed > 130*FRACUNIT*watermul --max speed cap
						p.normalspeed = $ - p.normalspeed/50
					end
				elseif not (MRCE_isHyper(p) and not p.powers[pw_sneakers]) then
					if p.normalspeed > 85*FRACUNIT*watermul then --max speed cap
						p.normalspeed = $ - p.normalspeed/50
					end
				end
			end
			local momangle = R_PointToAngle2(0,0,p.rmomx,p.rmomy) //Used for angling new momentum in ability cases
			local pmom = FixedDiv(FixedHypot(p.rmomx,p.rmomy),p.mo.scale) //Current speed, scaled for normalspeed calculations
	

			//
			// Dummied out for MRCE.
			// ~Radicalicious (4/21/22)
			//
			/*
			//Knuckles momentum renewal
			if p.charability == CA_GLIDEANDCLIMB then
				//Create glide history
				if p.glidelast == nil then
					p.glidelast = 0
				end
				local gliding = p.pflags&PF_GLIDING
				local thokked = p.pflags&PF_THOKKED
				local exitglide = (p.glidelast == 1 and not(gliding) and thokked)
				local landglide = (p.glidelast == 2 and not(gliding|thokked))
				//Restore glide momentum after deactivation
				if exitglide or landglide then
					p.mo.momx = p.xmlastmomx
					p.mo.momy = p.xmlastmomy
				end
				//Update glide history
				if gliding then
					p.glidelast = 1 //Gliding
				elseif exitglide then
					p.glidelast = 2 //Falling from glide
				elseif not(gliding|thokked) then
					p.glidelast = 0 //Not in glide state
				end
			end
				
			//////
			//Fang momentum renewal
			if p.charability == CA_BOUNCE then
				//Create bounce history
				if p.bouncelast == nil then
					p.bouncelast = false
				end
				if p.pflags&PF_BOUNCING and not(p.bouncelast) //Activate bounce
					or (not(p.pflags&PF_BOUNCING) and p.pflags&PF_THOKKED and p.bouncelast) //Deactivate bounce
					//Undo the momentum cut from bounce activation/deactivation
					p.mo.momx = p.xmlastmomx
					p.mo.momy = p.xmlastmomy
					p.mo.momz = $*2
				end
				//Update bounce history
				p.bouncelast = (p.pflags&PF_BOUNCING > 0)
			end
			*/
		p.xmlastspeed = p.speed
		p.xmlastz = p.mo.z
		p.xmlastx = p.mo.x
		p.xmlasty = p.mo.y
		p.xmlastmomx = p.mo.momx
		p.xmlastmomy = p.mo.momy
		p.xmlaststate = p.mo.state
	end
end)

addHook("PlayerThink", function(p)
	local x = p.mrce
--detect if physics are enabled
	if IsCustomSkin(p) then
		x.physics = false
	else
		x.physics = true
	end
--disable custom physics for a few known characters
	if p.mo and ((p.mo.skin == "sms") or (p.mo.skin == "ass") or (p.mo.skin == "juniosonic") or (p.mo.skin == "iclyn") or (p.mo.skin =="kiryu") or (p.mo.skin == "adventuresonic")) then
		x.customskin = 2
		x.physics = false
	elseif x.dontwantphysics == false then
		x.physics = true
	end
	--if p.speed == go fast, allow the player to run on water. aka, momentum based water running
	if (x.realspeed >= 60*FRACUNIT) and not (skins[p.mo.skin].flags & SF_RUNONWATER)  and not IsCustomSkin(p) 
	and not ((p.mo.skin == "mario" or p.mo.skin == "luigi") and p.powers[pw_shield] == SH_MINI) then --mini mario gets to always run on water
		p.charflags = $1|SF_RUNONWATER
	elseif not IsCustomSkin(p) then
		if not (skins[p.mo.skin].flags & SF_RUNONWATER)
		and not ((p.mo.skin == "mario" or p.mo.skin == "luigi") and p.powers[pw_shield] == SH_MINI)
		and x.realspeed <= 60*FRACUNIT then
			p.charflags = $1 & ~(SF_RUNONWATER)
		end
	end
end)
